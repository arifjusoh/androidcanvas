package com.example.assestment;

public interface MainViewListener {
    void updateDisplayModeFab(int drawableId);
    void updateMarkFab(int drawableId);
    void updateFilterFab(int drawableId);
    void showFilterFab(boolean show);
}
