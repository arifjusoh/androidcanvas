package com.example.assestment.util;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;

import java.lang.reflect.Method;

public abstract class GeneralUtil {
    public static Activity getActivity(Context context) {
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }

    public static boolean respondsToSelector(Class aClass, String methodName) {
        boolean result = false;
        Class[] paramTypes = {};

        Method method = null;

        try {
            method = aClass.getMethod(methodName, paramTypes);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        if (method != null)
            result = true;

        return result;
    }
}
