package com.example.assestment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.assestment.board.BoardPresenter;
import com.example.assestment.util.GeneralUtil;

import java.util.List;

import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilter;

public class MainPresenter implements MainPresenterListener {
    private MainViewListener _view;
    private MainModal _modal;
    private Context _context;

    private BoardPresenter _boardPresenter;

    MainPresenter(MainViewListener view, Context context) {
        _view = view;
        _context = context;

        _modal = new MainModal();
    }

    @Override
    public void start() {
    }

    @Override
    public void stop() {

    }

    @Override
    public void onClickDisplayModeFab() {
        getBoardPresenter().onClickDisplayModeFab(new BoardPresenter.OnClickDisplayModeFabListener() {
            @Override
            public void onComplete(int displayMode) {
                int drawableId = getModal().getDisplayModeDrawableId(displayMode);
                getView().updateDisplayModeFab(drawableId);
            }
        });
    }

    @Override
    public void onClickMarkFab(String tag, int resoureId) {
        Bitmap bitMap = BitmapFactory.decodeResource(getContext().getResources(), resoureId);
        getBoardPresenter().onClickMarkFab(tag, bitMap, new BoardPresenter.OnClickMarkFabListener() {
            @Override
            public void onComplete() {
                getView().updateMarkFab(getModal().getMarkStateDrawableId(!getModal().isMarkState()));

                if (!getModal().isMarkState()) {
                    getView().updateFilterFab(getModal().getFilterStateDrawableId(false));
                }

                getView().showFilterFab(getModal().isMarkState());
            }

            @Override
            public void onFailed() {
                // todo: show alert when error
            }
        });
    }

    @Override
    public void onClickFilterFab(String tag, List<GPUImageFilter> filters) {
        getBoardPresenter().onClickFilterFab(tag, filters, new BoardPresenter.OnClickFilterFabListener() {
            @Override
            public void onComplete() {
                getView().updateFilterFab(getModal().getFilterStateDrawableId(!getModal().isFilterFabState()));
            }

            @Override
            public void onFailed() {
                // todo: show alert when error
            }
        });
    }

    // Accessor
    private BoardPresenter getBoardPresenter(){
        if (_boardPresenter == null)
            _boardPresenter = ((MainActivity)GeneralUtil.getActivity(getContext())).getBoardPresenter();

        return _boardPresenter;
    }

    private MainViewListener getView() {
        return _view;
    }

    private Context getContext() {
        return _context;
    }

    private MainModal getModal() {
        return _modal;
    }
}
