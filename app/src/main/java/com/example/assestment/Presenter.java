package com.example.assestment;

public interface Presenter {
    void start();
    void stop();
}
