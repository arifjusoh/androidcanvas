package com.example.assestment;

class MainModal {
    private boolean _markFabState;
    private boolean _filterFabState;
    int getDisplayModeDrawableId(int style) {
        switch (style) {
            case GeneralConstant.displayMode3X3:
                return android.R.drawable.ic_menu_more;

            case GeneralConstant.displayMode2X2:
            default:
                return android.R.drawable.ic_dialog_dialer;
        }
    }

    int getMarkStateDrawableId(boolean state) {
        setMarkState(state);
        return isMarkState() ? android.R.drawable.checkbox_on_background : android.R.drawable.checkbox_off_background;
    }

    int getFilterStateDrawableId(boolean state) {
        setFilterFabState(state);
        return isFilterFabState() ? android.R.drawable.ic_menu_delete : android.R.drawable.ic_menu_add;
    }

    // Mutator methods
    private void setMarkState(boolean markState) {
        _markFabState = markState;
    }

    private void setFilterFabState(boolean filterFabState) {
        _filterFabState = filterFabState;
    }

    // Accessor methods
    boolean isMarkState() {
        return _markFabState;
    }

    boolean isFilterFabState() {
        return _filterFabState;
    }
}
