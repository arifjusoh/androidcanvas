package com.example.assestment;

import java.util.List;

import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilter;

public interface MainPresenterListener extends Presenter {
    void onClickDisplayModeFab();
    void onClickMarkFab(String tag, int resourceId);
    void onClickFilterFab(String tag, List<GPUImageFilter> list);
}
