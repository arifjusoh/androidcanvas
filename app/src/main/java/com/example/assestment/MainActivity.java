package com.example.assestment;

import android.graphics.BitmapFactory;
import android.os.Bundle;

import com.example.assestment.board.BoardPresenter;
import com.example.assestment.board.BoardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageLookupFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageSepiaToneFilter;

public class MainActivity extends AppCompatActivity implements MainViewListener, View.OnClickListener {

    private MainPresenter _mainPresenter;
    private BoardPresenter _boardPresenter;

    private FloatingActionButton _displayModeFab;
    private FloatingActionButton _markFab;
    private FloatingActionButton _filterFab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getDisplayModeFab().setOnClickListener(this);
        getMarkFab().setOnClickListener(this);
        getFilterFab().setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // MainViewListener
    @Override
    public void updateDisplayModeFab(int drawableId) { // 2x2 android.R.drawable.ic_menu_more 3x3 android.R.drawable.ic_dialog_dialer
        getDisplayModeFab().setImageResource(drawableId);
    }

    @Override
    public void updateMarkFab(int drawableId) {
        getMarkFab().setImageResource(drawableId);
    }

    @Override
    public void updateFilterFab(int drawableId) {
        getFilterFab().setImageResource(drawableId);
    }

    @Override
    public void showFilterFab(boolean show) {
        getFilterFab().setVisibility(show ? View.VISIBLE : View.GONE);
    }

    // View.OnClickListener
    @Override
    public void onClick(View v) {
        if (v.equals(getDisplayModeFab())){
            getMainPresenter().onClickDisplayModeFab();
        } else if (v.equals(getMarkFab())) {
            getMainPresenter().onClickMarkFab(getMarkFab().getTag().toString(), R.drawable.ic_android);
        } else if (v.equals(getFilterFab())) {
            List<GPUImageFilter> filters = new ArrayList<>();
            filters.add(getSepiaToneFilter());
            filters.add(getAlphaBlendFilter());
            filters.add(getLookupFilter());
            
            getMainPresenter().onClickFilterFab(getFilterFab().getTag().toString(), filters);
        }
    }
    private GPUImageFilter getSepiaToneFilter() {
        GPUImageSepiaToneFilter filter = new GPUImageSepiaToneFilter();
        filter.setIntensity(1.0f);

        return filter;
    }

    private GPUImageFilter getLookupFilter() {
        GPUImageLookupFilter filter = new GPUImageLookupFilter();
        filter.setBitmap(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.lookup_amatorka));

        return filter;
    }

    private GPUImageFilter getAlphaBlendFilter() {
        GPUImageSepiaToneFilter filter = new GPUImageSepiaToneFilter();
        filter.setIntensity(1.5f);
        return filter;
    }

    // Accessor methods
    public FloatingActionButton getFilterFab() {
        if (_filterFab == null)
            _filterFab = findViewById(R.id.filterFab);
        return _filterFab;
    }

    public FloatingActionButton getDisplayModeFab() {
        if (_displayModeFab == null)
            _displayModeFab = findViewById(R.id.displayModeFab);

        return _displayModeFab;
    }

    public FloatingActionButton getMarkFab() {
        if (_markFab == null)
            _markFab = findViewById(R.id.markFab);

        return _markFab;
    }

    public MainPresenter getMainPresenter() {
        if (_mainPresenter == null)
            _mainPresenter = new MainPresenter(this, this);
        return _mainPresenter;
    }

    public BoardPresenter getBoardPresenter() {
        if (_boardPresenter == null) {
            BoardView mainContentView = findViewById(R.id.mainView);
            _boardPresenter = new BoardPresenter(mainContentView);
        }
        return _boardPresenter;
    }
}
