package com.example.assestment.board;

import android.graphics.Bitmap;

import com.example.assestment.Presenter;

import java.util.List;

import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilter;

public interface BoardPresenterListener extends Presenter {
    void onDraw();
    void onSizeChange(int width, int height);

    void onClickDisplayModeFab(BoardPresenter.OnClickDisplayModeFabListener listener);
    void onClickMarkFab(String cellPath, Bitmap bitmap, BoardPresenter.OnClickMarkFabListener listener);
    void onClickFilterFab(String cellPath, List<GPUImageFilter> list, BoardPresenter.OnClickFilterFabListener listener);
}
