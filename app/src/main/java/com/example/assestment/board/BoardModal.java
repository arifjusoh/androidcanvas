package com.example.assestment.board;

import android.graphics.Bitmap;
import android.graphics.Rect;

import com.example.assestment.GeneralConstant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class BoardModal {
    private int _displayMode;

    private int _gapX;
    private int _gapY;
    private int _numberOfRows;
    private int _numberOfColumns;
    private int _cellWidth;
    private int _cellHeight;
    private Map<String, Cell> _cellMap;

    private int _containerWidth;
    private int _containerHeight;

    private int _selectedDisplayModeIndex;

    private int[] _displayModeList;

    BoardModal() {
        _cellHeight = 150;
        _cellWidth = 150;
        toggleDisplayMode(0);
    }

    /**
     * method for presenter to toggle the display mode
     */
    void toggleDisplayMode() {
        toggleDisplayMode(getSelectedDisplayModeIndex() + 1);
    }

    /**
     * to change current display mode to specify mode index given
     *
     * @param index index of display mode list
     */
    private void toggleDisplayMode(int index) {
        index = (index < getDisplayModeList().length) ? index : 0;

        setSelectedDisplayModeIndex(index);

        setDisplayMode(getDisplayModeList()[getSelectedDisplayModeIndex()]);

        measureGap();
        prepareCellMap();
    }

    boolean toggleImage(String cellPath, Bitmap bitmap) {
        if (cellPath != null) {
            try {
                Cell cell = getCellMap().get(cellPath);
                if (cell != null) {
                    if (cell.getBitmap() != null){
                        cell.setBitmap(null);
                        cell.setFilteredBitmap(null);
                    } else {
                        if (bitmap == null)
                            return false;
                        cell.setBitmap(bitmap);
                    }

                    return true;
                }
            } catch (NullPointerException | ClassCastException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    boolean toggleImageFilter(String cellPath) {
        if (cellPath != null) {
            try {
                Cell cell = getCell(cellPath);
                if (cell != null && cell.getBitmap() != null) {
                    if (cell.getFilteredBitmap() != null) {
                        cell.setFilteredBitmap(null);
                        return false;
                    }

                    return true;
                }
            } catch (NullPointerException | ClassCastException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    void updateSize(int width, int height) {
        setContainerWidth(width);
        setContainerHeight(height);

        measureGap();
        prepareCellMap();
    }

    private void measureGap() {
        int gapTotalWidth = getContainerWidth() - getCellWidth() * getNumberOfColumns();
        int gapTotalHeight = getContainerHeight() - getCellHeight() * getNumberOfRows();

        int gapXCount = getNumberOfColumns() + 1;
        int gapYCount = getNumberOfRows() + 1;

        setGapX(gapTotalWidth / gapXCount);
        setGapY(gapTotalHeight / gapYCount);
    }

    private void prepareCellMap() {
        int gapX = getGapX();
        int gapY = getGapY();
        int cellWidth = getCellWidth();
        int cellHeight = getCellHeight();

        Rect rect = new Rect();

        for (int rowIndex = 0; rowIndex < getNumberOfRows(); rowIndex++) {
            if (rowIndex == 0) {
                rect.top = gapY;
                rect.bottom = rect.top + cellHeight;
            } else {
                rect.top = rect.top + cellHeight + gapY;
                rect.bottom = rect.top + cellHeight;
            }

            for (int columnIndex = 0; columnIndex < getNumberOfColumns(); columnIndex++) {
                if (columnIndex == 0) {
                    rect.left = gapX;
                    rect.right = rect.left + cellWidth;
                } else {
                    rect.left = rect.left + cellWidth + gapX;
                    rect.right = rect.left + cellWidth;
                }

                String cellPath = rowIndex + "-" + columnIndex;
                Cell cell = getCell(cellPath);

                if (cell != null) {
                    cell.setRect(rect);
                } else {
                    cell = new Cell(rect);
                    _cellMap.put(cellPath, cell);
                }
            }
        }
    }

    List<Cell> getDisplayList() {
        List<Cell> list = new ArrayList<Cell>();
        for (int rowIndex = 0; rowIndex < getNumberOfRows(); rowIndex++) {
            for (int columnIndex = 0; columnIndex < getNumberOfColumns(); columnIndex++) {
                String cellPath = rowIndex + "-" + columnIndex;
                list.add(getCell(cellPath));
            }
        }
        return list;
    }

    // mutator methods
    private void setDisplayMode(int displayMode) {
        _displayMode = displayMode;

        switch (getDisplayMode()) {
            case GeneralConstant.displayMode3X3: {
                _numberOfColumns = 3;
                _numberOfRows = 3;
            }
            break;

            case GeneralConstant.displayMode2X2:
            default: {
                _numberOfColumns = 2;
                _numberOfRows = 2;
            }
            break;
        }
    }

    private void setSelectedDisplayModeIndex(int selectedStyle) {
        _selectedDisplayModeIndex = selectedStyle;
    }

    private void setContainerWidth(int containerWidth) {
        _containerWidth = containerWidth;
    }

    private void setContainerHeight(int containerHeight) {
        _containerHeight = containerHeight;
    }

    private void setGapX(int gapX) {
        _gapX = gapX;
    }

    private void setGapY(int gapY) {
        _gapY = gapY;
    }

    // accessor methods
    Map<String, Cell> getCellMap() {
        if (_cellMap == null)
            _cellMap = new HashMap<>();
        return _cellMap;
    }

    private int[] getDisplayModeList() {
        if (_displayModeList == null) {
            _displayModeList = new int[]{
                    GeneralConstant.displayMode2X2,
                    GeneralConstant.displayMode3X3
            };
        }
        return _displayModeList;
    }

    private int getSelectedDisplayModeIndex() {
        return _selectedDisplayModeIndex;
    }

    Cell getCell(String cellPath) {
        try {
            return getCellMap().get(cellPath);
        } catch (NullPointerException | ClassCastException e) {
            e.printStackTrace();
        }
        return null;
    }
    private int getContainerWidth() {
        return _containerWidth;
    }

    private int getContainerHeight() {
        return _containerHeight;
    }

    private int getCellWidth() {
        return _cellWidth;
    }

    private int getCellHeight() {
        return _cellHeight;
    }

    private int getNumberOfColumns() {
        return _numberOfColumns;
    }

    private int getNumberOfRows() {
        return _numberOfRows;
    }

    int getDisplayMode() {
        return _displayMode;
    }

    private int getGapX() {
        return _gapX;
    }

    private int getGapY() {
        return _gapY;
    }
}
