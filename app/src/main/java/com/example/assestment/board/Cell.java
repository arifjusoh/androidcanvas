package com.example.assestment.board;

import android.graphics.Bitmap;
import android.graphics.Rect;

class Cell {
    private Rect _rect;

    private Bitmap _bitmap;

    private boolean _filteringImage;
    private Bitmap _filteredBitmap;

    Cell(Rect rect) {
        setRect(rect);
    }

    // Mutator Methods
    void setRect(Rect rect) {
        _rect = new Rect(rect);
    }
    void setBitmap(Bitmap bitmap) {
        _bitmap = bitmap;
    }

    void setFilteredBitmap(Bitmap filteredBitmap) {
        _filteredBitmap = filteredBitmap;
    }

    void setFilteringImage(boolean filteringImage) {
        _filteringImage = filteringImage;
    }

    // Accessor methods

    Bitmap getBitmap() {
        return _bitmap;
    }

    boolean isFilteringImage() {
        return _filteringImage;
    }

    Bitmap getFilteredBitmap() {
        return _filteredBitmap;
    }

    Rect getRect() {
        return _rect;
    }
}
