package com.example.assestment.board;

import android.graphics.Bitmap;

import java.util.List;

import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilter;

public class BoardPresenter implements BoardPresenterListener {
    private BoardModal _modal;
    private BoardViewListener _view;
    private OnClickDisplayModeFabListener _displayModeFabListener;
    private OnClickMarkFabListener _markFabListener;
    private OnClickFilterFabListener _filterFabListener;

    public BoardPresenter(BoardViewListener view) {
        _view = view;
        _modal = new BoardModal();
    }

    @Override
    public void onClickDisplayModeFab(OnClickDisplayModeFabListener listener) {
        setDisplayModeFabListener(listener);
        getModal().toggleDisplayMode();
        getView().reset();
    }

    @Override
    public void onClickMarkFab(String cellPath, Bitmap bitmap, OnClickMarkFabListener listener) {
        if (getModal().toggleImage(cellPath, bitmap)) {
            setMarkFabListener(listener);
            getView().reset();
        } else {
            listener.onFailed();
        }
    }

    @Override
    public void onClickFilterFab(String cellPath, List<GPUImageFilter> list, OnClickFilterFabListener listener) {
        setFilterFabListener(listener);
        if (getModal().toggleImageFilter(cellPath)) {
            getView().applyFilters(getModal().getCell(cellPath), list);
        }

        getView().reset();
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void onSizeChange(int width, int height) {
        getModal().updateSize(width, height);
    }

    @Override
    public void onDraw() {
        getView().drawBoard(getModal().getDisplayList(), new BoardView.OnDrawBoardListener() {
            @Override
            public void onDrawCompleted() {
                if (getDisplayModeFabListener() != null){
                    int displayMode = getModal().getDisplayMode();
                    getDisplayModeFabListener().onComplete(displayMode);
                    setDisplayModeFabListener(null);
                } else if (getMarkFabListener() != null) {
                    getMarkFabListener().onComplete();
                    setMarkFabListener(null);
                } else if (getFilterFabListener() != null) {
                    getFilterFabListener().onComplete();
                    setFilterFabListener(null);
                }
            }
        });
    }

    // Mutator methods
    private void setDisplayModeFabListener(OnClickDisplayModeFabListener displayModeFabListener) {
        _displayModeFabListener = displayModeFabListener;
    }

    private void setMarkFabListener(OnClickMarkFabListener markFabListener) {
        _markFabListener = markFabListener;
    }

    private void setFilterFabListener(OnClickFilterFabListener filterFabListener) {
        _filterFabListener = filterFabListener;
    }

    // Accessor methods
    private BoardModal getModal() {
        return _modal;
    }

    private BoardViewListener getView() {
        return _view;
    }

    private OnClickDisplayModeFabListener getDisplayModeFabListener() {
        return _displayModeFabListener;
    }

    private OnClickMarkFabListener getMarkFabListener() {
        return _markFabListener;
    }

    private OnClickFilterFabListener getFilterFabListener() {
        return _filterFabListener;
    }

    // Listener
    public interface OnClickDisplayModeFabListener {
        void onComplete(int displayMode);
    }

    public interface OnClickMarkFabListener {
        void onComplete();
        void onFailed();
    }

    public interface OnClickFilterFabListener {
        void onComplete();
        void onFailed();
    }
}
