package com.example.assestment.board;

import java.util.List;

import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilter;

public interface BoardViewListener {
    void drawBoard(List<Cell> data, BoardView.OnDrawBoardListener listener);
    void reset();
    void applyFilters(Cell cell, List<GPUImageFilter> filters);
}
