package com.example.assestment.board;

import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

import com.example.assestment.MainActivity;
import com.example.assestment.util.GeneralUtil;

import java.util.List;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilter;

public class BoardView extends View implements BoardViewListener {
    private MainActivity _mainActivity;
    private Canvas _canvas;
    private GPUImage _gpuImage;

    public BoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldW, int oldH) {
        super.onSizeChanged(w, h, oldW, oldH);

        getBoardPresenter().onSizeChange(w, h);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        setCanvas(canvas);

        getBoardPresenter().onDraw();
    }

    // BoardViewListener override methods
    @Override
    public void drawBoard(List<Cell> data, OnDrawBoardListener listener) {
        if (getCanvas() == null)
            return;

        getCanvas().drawColor(Color.WHITE);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);

        for(Cell cell : data) {

            Bitmap bitmap;
            if (cell.getFilteredBitmap() != null) {
                bitmap = cell.getFilteredBitmap();
            } else if (cell.getBitmap() != null) {
                bitmap = cell.getBitmap();
            } else {
                bitmap = null;
            }

            if (bitmap == null) {
                paint.setColor(Color.RED);
                getCanvas().drawRect(cell.getRect(), paint);
            } else  {
                getCanvas().drawBitmap(bitmap, null, cell.getRect(), new Paint());
            }
        }

        listener.onDrawCompleted();
    }

    @Override
    public void applyFilters(Cell cell, List<GPUImageFilter> filters) {
        cell.setFilteredBitmap(cell.getBitmap());

        for (GPUImageFilter filter : filters) {
            getGpuImage().setFilter(filter);
            cell.setFilteredBitmap(getGpuImage().getBitmapWithFilterApplied(cell.getFilteredBitmap()));
        }
    }

    @Override
    public void reset() {
        this.invalidate();
    }

    // Setter methods
    public void setCanvas(Canvas canvas) {
        _canvas = canvas;
    }

    //Accessor methods
    private MainActivity getMainActivity(){
        if (_mainActivity == null)
            _mainActivity =(MainActivity) GeneralUtil.getActivity(getContext());
        return _mainActivity;
    }

    public BoardPresenter getBoardPresenter() {
        return getMainActivity().getBoardPresenter();
    }


    public GPUImage getGpuImage() {
        if (_gpuImage == null) {
            _gpuImage = new GPUImage(getContext());

        }
        return _gpuImage;
    }

    public Canvas getCanvas() {
        return _canvas;
    }

    public interface OnDrawBoardListener {
        void onDrawCompleted();
    }
}
